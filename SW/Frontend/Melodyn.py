import os
import sys
import time
import numpy
import pygame
from pygame.locals import *
import yaml
from yaml.loader import SafeLoader
import musicpd

def read_config(filename):
    with open(filename) as f:
        data = yaml.load(f, Loader=SafeLoader)
    return data

def load_images(config):
    logos = numpy.empty(8, dtype=object)
    for media in config['media']:
        logos[media['position'] - 1] = pygame.image.load(media['logo'])
        if 'url' in media.keys():
            media_list[media['position'] - 1] = [action_radio,media['url']]
    return logos

def setup(config):
    cfg_fullscreen = config['gui']['fullscreen']
    if cfg_fullscreen:
        cfg_width = 0
        cfg_height = 0
    else:
        cfg_width = config['gui']['width']
        cfg_height = config['gui']['height']
    size = width, height = cfg_width, cfg_height
    if cfg_fullscreen:
        screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
    else:
        screen = pygame.display.set_mode(size)
    return screen

def screen_draw(screen):
    screen.fill(white)
    for i in range(0, len(logos)):
        if logos[i] != None:
            # Load images
            x = (i % 4) * 200
            y = (i // 4) * 200
            screen.blit(logos[i],(x,y))
            # Add click action
            if media_list[i] != None:
                actions.append([x, y, x+200, y+200, media_list[i][media_list_action], media_list[i][media_list_url]])
    pygame.display.flip()

def exec_action(mouse_x, mouse_y, mpc):
    for action in actions:
        if mouse_x > action[action_x1] and mouse_x < action[action_x2] and mouse_y > action[action_y1] and mouse_y < action[action_y2]:
            if action[action_type] == action_radio:
                mpc.connect()
                mpc.stop()
                mpc.clear()
                mpc.add(action[action_url])
                mpc.play()
                mpc.disconnect()
def worker():
    mpc = musicpd.MPDClient()
    while 1:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                
                exec_action(pygame.mouse.get_pos() [0], pygame.mouse.get_pos() [1], mpc)
            # ensure there is always a safe way to end the program if the touch screen fails
            if event.type == KEYDOWN:
                    if event.key == K_ESCAPE:
                            mpc.connect()
                            mpc.stop()
                            mpc.clear()
                            mpc.disconnect()
                            sys.exit()

    time.sleep(0.2)        
    pygame.display.update()


# Main
# define colours
blue = 26, 0, 255
cream = 254, 255, 25
black = 0, 0, 0
white = 255, 255, 255
yellow = 255, 255, 0
red = 255, 0, 0
green = 0, 255, 0

# Actions
action_radio = 1

# Media list index names
media_list_action = 0
media_list_url = 1

# Action definition index names
action_x1 = 0
action_y1 = 1
action_x2 = 2
action_y2 = 3
action_type = 4
action_url = 5

logos = numpy.empty(8, dtype=object)
media_list = numpy.empty(8, dtype=object)
actions = list()

config = read_config('./config.yaml')

logos = load_images(config)
screen = setup(config)
screen_draw(screen)
worker()
