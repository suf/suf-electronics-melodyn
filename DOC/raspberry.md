# Instructions
The instructions here are created for the Raspberry PI 3B+ with WaveShare 5" DSI IPS Capacitive Touch screen running RaspiOS integrated into the Videoton Melodyn R4900 radio. Maybe contains usefull informations for others also
# Setup RaspiOS
https://raspberrypi-guide.github.io/getting-started/install-operating-system.html

Setup Wifi  
Enable SSH  
Set Hungarian Keyboard  
Setup certificate authentication  
Set audio output  - raspi-config
# Setup mpd
```
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y mc mpd mpc
```
### Edit configuration /etc/mpd.conf
Add this (for the integrated audio jack):
```
audio_output {
        type            "alsa"
        name            "My ALSA Device"
        device          "hw:0,0"
}
```
# Setup Melodyn.py
```
sudo apt-get install python3 pip3
sudo pip install numpy PyYAML pygame python-musicpd
git clone https://gitlab.com/suf/suf-electronics-melodyn.git
sudo mkdir /opt/melodyn
sudo chmod 777 /opt/melodyn
cp -r suf-electronics-melodyn/SW/Frontend/* /opt/melodyn/
sudo chmod -r a+rw /opt/melodyn/*
sudo chmod 777 /opt/melodyn/Melodyn.py
```
Add the following into the ~/.profile
```
cd /opt/melodyn
python3 Melodyn.py
```

## Reference
[http://correderajorge.github.io/Music-Player-Daemon-Raspberry/]  
[https://www.blog.anupchhetri.com/?p=2159]  
[https://www.lesbonscomptes.com/pages/raspmpd.html]
