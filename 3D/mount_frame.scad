module mount_frame()
{
    corner_round=6;
    translate([0,0,-4])
    {
        difference()
        {
            translate([0,-4.5,-1])
            {                
                cube([185,144,10],center=true);
            }
            translate([-5,0,1.5])
            {
                cube([160,115,5.002],center=true);
            }
            translate([0,0,-1.5])
            {
                minkowski()
                {
                    cube([129-corner_round,89-corner_round,1.002],center=true);
                    cylinder(d=corner_round,h=0.001);
                }
            }
            translate([0,0,-4])
            {
                // cube([124.7,84.45,4.002],center=true);
                cube([123,80,4.002],center=true);
            }
            translate([30.8,-43,-6.001])
            {
                cube([15,5,4.002]);
            }
            // door lock mount
            translate([-75,0,-3.5])
            {
                cube([7.5,8.5,5.002],center=true);
            }
            translate([-75,0,-2.25])
            {
                cube([7.5,11.5,2.502],center=true);
            }
            // hinge mount
            for(j=[-44,-20,20,44])
            {                
                translate([75.5,j,-1])
                {                
                    cube([22.002,5,10.002],center=true);
                    translate([4.5,0,4])
                    {
                        rotate([90,0,0])
                        {
                            cylinder(d=5.5,h=11,center=true);
                        }
                    }
                }
            }
            // cover frame mount
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([88*i,-4.5+67.5*j,-1])
                    {
                        cylinder(d=3.5,h=10.002,center=true);
                    }
                }
            }
            // frame mount
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([78*i,-4.5+67.5*j,-1])
                    {
                        cylinder(d=3.5,h=10.002,center=true);
                    }
                }
            }
            // pi mount
            for(i=[-1,1])
            {
                for(j=[-1,1])
                {
                    translate([54.5*i,50*j,-1])
                    {
                        cylinder(d=3.5,h=10.002, center=true);
                        translate([0,0,-4])
                        {
                            cylinder(d=10,h=2.002, center=true);                            
                        }
                    }
                }
            }
            // USB connector
            translate([-25,-50,-1])
            {
                hull()
                {
                    for(i=[-1,1])
                    {
                        translate([15*i,0,-3.5])
                        {
                            cylinder(d=12.5,h=3.002, center=true);                            
                        }
                    }
                }
                for(i=[-1,1])
                {
                    translate([15*i,0,0])
                    {
                        cylinder(d=3.5,h=10.002, center=true);

                    }
                }
                cube([13.5,6.5,10.002],center=true);
            }
        }
    }
}
$fn=180;
translate([0,0,10])
{
    mount_frame();
}
