module door()
{
    difference()
    {
        translate([0,0,2.5])
        {
            hull()
            {
                translate([-0.25,0,0])
                {
                    cube([154.5,113,5],center=true);
                }
                translate([0,0,2.499])
                {
                    cube([158.5,113.5,0.001],center=true);
                }
            }
            // door lock
            translate([-70,0,-5.25])
            {
                cube([2.5,4,5.5],center=true);
            }
            translate([-70,0,-3.5])
            {
                difference()
                {
                    cube([6.5,8,2],center=true);
                    for(i=[-1,1])
                    {
                        translate([0,4*i,-1])
                        {
                            rotate([0,90,0])
                            {
                                cylinder(d=4,h=6.502,center=true);
                            }
                        }
                    }
                    for(i=[-1,1])
                    {
                        translate([3.25*i,0,-1])
                        {
                            rotate([90,0,0])
                            {
                                cylinder(d=4,h=8.002,center=true);
                            }
                        }
                    }
                }
                
            }
            hull()
            {
                translate([-70,0,-10.5])
                {
                    cube([2.5,4,0.001],center=true);
                }
                translate([-70,0,-8])
                {
                    cube([2.5,5.7,0.001],center=true);
                }
            }
        }
        // grill
        for(i=[-6:1:6])
        {
            j = abs(i) < 2 ? 10 : 0;
            translate([0,i*8,0])
            {
                hull()
                {
                    translate([-70 + j,0,-0.001])
                        cylinder(d=4, h=5.002);
                    translate([70,0,-0.001])
                        cylinder(d=4, h=5.002);
                }
            }
        }
    }
    // hinge
    for(j=[-44,-20,20,44])
    {
        translate([81.5,j,-5.77058])
        {
            rotate([90,0,0])
            {
                difference()
                {
                    cylinder(d=18,h=4, center=true);
                    cylinder(d=9,h=4.002, center=true);
                    translate([-9,0,-2.001])
                    {
                        cube([9.001,9.001,4.002]);
                    }
                    rotate([0,0,-31.2])
                    {
                        translate([-9,0,-2.001])
                        {
                            cube([9.001,9.001,4.002]);
                        }
                    }
                }
                translate([-9,0,-2])
                {
                    cube([4.5,9,4]);
                }
                difference()
                {
                    union()
                    {
                        /*
                        translate([-4.5,5.27058,-4])
                        {
                            #cube([0.5,0.5,8]);
                        }
                        */
                        translate([-13,3.77058,-4])
                        {
                            cube([8.5,2,8]);
                        }
                        translate([-13,1.77058,-2])
                        {                    
                            cube([4,4,4]);
                        }

                    }
                    for(i=[-1,1])
                    {
                        translate([-13.001,3.77058,-4*i])
                        {                    
                            rotate([0,90,0])
                            {                
                                cylinder(d=4,h=9.002);
                            }
                            translate([1.5,0,0])
                            {
                                rotate([-90,0,0])
                                {
                                    cylinder(d=4,h=2.001);
                                }                    
                            }
                            translate([0.75,1.001,i])
                            {                    
                                cube([1.5,2,2],center=true);
                            }
                        }
                    }
                    /*
                    translate([-4,5.27058,-4.001])
                    {
                        #cylinder(d=1,h=8.002);
                    }
                    */
                    translate([-13,1.77058,-4.001])
                    {
                        cylinder(d=8,h=8.002);
                    }
                }
            }
        }
        translate([85,j,0])
        {
            rotate([90,0,0])
            {    
                // cylinder(d=4.5,h=4,center=true);
                cylinder(d=4.5,h=10,center=true);
            }
        }
    }
}

$fn=180;
translate([0,0,5])
{
    rotate([180,0,0])
    {
        door();
    }
}