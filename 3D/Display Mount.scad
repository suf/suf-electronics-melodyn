$fn=180;
door_open=$t*90;
corner_round=6;
use <cover_frame.scad>
use <door.scad>
use <mount_frame.scad>
use <pi_mount.scad>
use <back_support.scad>

module controler()
{
    translate([0,0,37])
    {
        minkowski()
        {
            cube([127.7-corner_round,87.45-corner_round,1],center=true);
            cylinder(d=corner_round,h=0.001);
        }
    }
    for(i=[-1,1])
    {
        for(j=[-1,1])
        {
            translate([57.5*i,32.9*j])
            {
                cylinder(d=6,h=36.5);
            }
        }
    }
}


translate([0,37.5,135/2])
{
    rotate([90,0,0])
    {
        controler();
    }
}

translate([0,-5,135/2])
{
    rotate([90,0,0])
    {
        color("beige")
        {        
            cover_frame();
        }
    }
}

translate([0,-5,135/2])
{
    rotate([90,0,0])
    {
        color("blue")
        {
            mount_frame();
        }
    }
}


translate([-5,-5,135/2])
{
    rotate([90,0,0])
    {
        translate([85,0,0])
        {
            rotate([0,door_open,0])
            {
                translate([-85,0,0])
                {
                    color("green")
                    {
                        door();
                    }
                }
            }
        }
    }
}
translate([0,37.5,135/2])
{
    rotate([90,0,0])
    {
        for(i=[-1,1])
        {
            for(j=[-1,1])
            {
                translate([54.5*i,50*j])
                {
                    cylinder(d=6,h=35);
                }
            }
        }
    }
}

translate([0,40.5,135/2])
{
    rotate([90,0,0])
    {
        color("red")
        {
            pi_mount();
        }
    }
}
translate([88,12.8,135/2])
{
    rotate([90,0,0])
    {
        color("gray")
        {
            back_support();
        }
    }
}